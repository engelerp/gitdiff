# gitdiff

A script to generate a compiled latexdiff file from different git commits.

Install to some standard location (on macOS: /usr/local/bin/).

Issue 'gitdiff --help' for it to explain itself.