#!/bin/bash

NUMARGS=$#

if (($NUMARGS == 0))
then
        echo -e "Info:\ngitdiff generates a compiled latexdiff file from different git commits."
        echo -e "Usage:\ngitdiff <other hash> filename.tex\ngitdiff <new hash> <old hash> filename.tex\ngitdiff <new hash> <old hash> <path to file> filename.tex"
fi
if (($NUMARGS == 1))
then
        echo -e "Info:\ngitdiff generates a compiled latexdiff file from different git commits."
        echo -e "Usage:\ngitdiff <other hash> filename.tex\ngitdiff <new hash> <old hash> filename.tex\ngitdiff <new hash> <old hash> <path to file> filename.tex"
fi
if (($NUMARGS == 2))
then
        GITSTATUS=$(git diff --stat)
        if [ -z "$GITSTATUS" ]
        then
                :
        else
                echo "Please clean your working tree first."
                exit 1
        fi
        CURRENTHASH=$(git rev-parse HEAD)
        OTHERHASH=$1
        FILENAME=$2
        TEMPDIR=${CURRENTHASH}_${OTHERHASH}_GITDIFF_TEMPDIR_314159
        DIFFFILE=gitdiff_${CURRENTHASH}_${OTHERHASH}

        if [ -f "$DIFFFILE" ]
        then
                echo -e "Please remove $DIFFFILE first."
                exit 1
        fi
        if [ -d "$TEMPDIR" ]
        then
                echo -e "Please remove $TEMPDIR first"
                exit 1
        fi

        if git cat-file -e $OTHERHASH^{commit}
        then
                echo -e "$OTHERHASH is valid"
        else
                echo -e "Fatal: Hash $OTHERHASH is invalid"
                exit 1
        fi
        git checkout $OTHERHASH
        if [ -f "$FILENAME" ]
        then
                :
        else
                echo "Fatal: $FILENAME does not exist"
                git checkout -
                exit 1
        fi
        mkdir $TEMPDIR
        cp $FILENAME $TEMPDIR
        git checkout -
        latexdiff $TEMPDIR/$FILENAME $FILENAME > $DIFFFILE.tex
        pdflatex $DIFFFILE.tex
        bibtex $DIFFFILE.tex
        pdflatex $DIFFFILE.tex
        rm -rf $DIFFFILE.aux $DIFFFILE.out $DIFFFILE.log
        #rm -rf $DIFFFILE.tex
        rm -rf $TEMPDIR
fi
if (($NUMARGS == 3))
then
        GITSTATUS=$(git diff --stat)
        if [ -z "$GITSTATUS" ]
        then
                :
        else
                echo "Please clean your working tree first."
                exit 1
        fi
        CURRENTHASH=$(git rev-parse HEAD)
        NEWHASH=$1
        OLDHASH=$2
        FILENAME=$3
        TEMPDIR=${NEWHASH}_${OLDHASH}_GITDIFF_TEMPDIR_314159
        DIFFFILE=gitdiff_${NEWHASH}_${OLDHASH}

        if [ -f "$DIFFFILE" ]
        then
                echo -e "Please remove $DIFFFILE first."
                exit 1
        fi
        if [ -d "$TEMPDIR" ]
        then
                echo -e "Please remove $TEMPDIR first"
                exit 1
        fi

        if git cat-file -e $OLDHASH^{commit}
        then
                echo -e "$OLDHASH is valid"
        else
                echo -e "Fatal: Hash $OLDHASH is invalid"
                exit 1
        fi
        if git cat-file -e $NEWHASH^{commit}
        then
                echo -e "$NEWHASH is valid"
        else
                echo -e "Fatal: Hash $NEWHASH is invalid"
                exit 1
        fi
        git checkout $OLDHASH
        if [ -f "$FILENAME" ]
        then
                :
        else
                echo "Fatal: $FILENAME does not exist"
                git checkout -
                exit 1
        fi
        mkdir $TEMPDIR
        cp $FILENAME $TEMPDIR/$FILENAME
        git checkout -
        git checkout $NEWHASH
        if [ -f "$FILENAME" ]
        then
                :
        else
                echo "Fatal: $FILENAME does not exist"
                rm -rf $TEMPDIR
                git checkout -
                exit 1
        fi
        latexdiff $TEMPDIR/$FILENAME $FILENAME > $DIFFFILE.tex
        git checkout -
        pdflatex $DIFFFILE.tex
        bibtex $DIFFFILE.tex
        pdflatex $DIFFFILE.tex
        rm -rf $DIFFFILE.aux $DIFFFILE.out $DIFFFILE.log
        #rm -rf $DIFFFILE.tex
        rm -rf $TEMPDIR
fi
if (($NUMARGS == 4))
then
        CURRENTDIR=$(pwd)
        NEWHASH=$1
        OLDHASH=$2
        DIRECTORY=$3
        FILENAME=$4
        if [ -d "$DIRECTORY" ]
        then
                :
        else
                echo -e "Fatal: $DIRECTORY doesn't exist"
                exit 1
        fi
        if $(git -C $DIRECTORY rev-parse)
        then
                :
        else
                echo "Fatal: Not a git repository"
                cd $CURRENTDIR
                exit 1
        fi
        cd $DIRECTORY
        GITSTATUS=$(git diff --stat)
        if [ -z "$GITSTATUS" ]
        then
                :
        else
                echo "Please clean your working tree first."
                cd $CURRENTDIR
                exit 1
        fi
        if $(git cat-file -e $NEWHASH^{commit})
        then
                echo -e "$NEWHASH is valid"
        else
                echo -e "Fatal: Hash $NEWHASH is invalid"
                cd $CURRENTDIR
                exit 1
        fi
        if $(git cat-file -e $OLDHASH^{commit})
        then
                echo -e "$OLDHASH is valid"
        else
                echo -e "Fatal: Hash $OLDHASH is invalid"
                cd $CURRENTDIR
                exit 1
        fi


        CURRENTHASH=$(git rev-parse HEAD)
        TEMPDIR=${NEWHASH}_${OLDHASH}_GITDIFF_TEMPDIR_314159
        DIFFFILE=gitdiff_${NEWHASH}_${OLDHASH}

        if [ -d "$TEMPDIR" ]
        then
                echo -e "Please remove $TEMPDIR from the repo first"
                cd $CURRENTDIR
                exit 1
        fi

        git checkout $OLDHASH
        if [ -f "$FILENAME" ]
        then
                :
        else
                echo "Fatal: $FILENAME does not exist"
                git checkout -
                cd $CURRENTDIR
                exit 1
        fi
        mkdir $TEMPDIR
        cp $FILENAME $TEMPDIR/$FILENAME
        git checkout -
        git checkout $NEWHASH
        if [ -f "$FILENAME" ]
        then
                :
        else
                echo "Fatal: $FILENAME does not exist"
                rm -rf $TEMPDIR
                git checkout -
                cd $CURRENTDIR
                exit 1
        fi
        latexdiff $TEMPDIR/$FILENAME $FILENAME > $DIFFFILE.tex
        git checkout -
        pdflatex $DIFFFILE.tex
        bibtex $DIFFFILE.tex
        pdflatex $DIFFFILE.tex
        rm -rf $DIFFFILE.aux $DIFFFILE.out $DIFFFILE.log
        #rm -rf $DIFFFILE.tex
        rm -rf $TEMPDIR
        mv $DIFFFILE.pdf $CURRENTDIR/${FILENAME//.}_$DIFFFILE.pdf
        cd $CURRENTDIR
fi
